const express = require('express')
const router = express.Router()
const pool = require('../../../database/connect')

/**
 * @openapi
 * /api:
 *   get:
 *     description: Test that the API is working properly.
 *     responses:
 *       200:
 *         description: Returns a string confirming that the API is online and connected to a MariaDB database.
 *       500:
 *         description: Returns error details.
 */
 router.get('/', async (req, res) => {
    const dbConnection = await pool.getConnection()
    try {
        const result = await dbConnection.query('SELECT NOW()', (err, rows) => {
            if (err) {
                throw err
            } else {
                return rows
            }
          })
        const timeNow = result[0]['NOW()']
        return res.status(200).send(`${process.env.API_NAME} is online. Database says that the current time is: ${timeNow}.`)
    } catch (err) {
        return res.status(500).send({
            success: false,
            message: `Error getting response from ${process.env.API_NAME}.`,
            error: err,
        })
    } finally {
        const dateNow = new Date
        const clientIP = req.headers['x-forwarded-for'] || req.socket.remoteAddress || null
        console.log(`${dateNow.toISOString()} | ${req.method} ${req.baseUrl} | Status: ${res.statusCode} | IP: ${clientIP} | User-Agent: ${req.headers['user-agent']}`)
        if (dbConnection) return dbConnection.end()
      }
})

module.exports = router
