const express = require('express')
const router = express.Router()
const itemController = require('../../controllers/item/index.js')

/**
 * @openapi
 * /api/item:
 *   get:
 *     description: Get some hardcoded items.
 *     responses:
 *       200:
 *         description: Returns a list of five items.
 *       500:
 *         description: Returns error details.
 */
router.get('/', itemController.getAllItems)

module.exports = router