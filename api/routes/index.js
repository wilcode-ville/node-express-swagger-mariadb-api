const express = require('express')
const router = express.Router()

const apiRoute = '/api'

// Root
router.use(`${apiRoute}`, require('./root/index.js'))

// Item
router.use(`${apiRoute}/item`, require('./item/index.js'))

module.exports = router