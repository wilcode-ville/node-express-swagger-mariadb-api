const itemController = {}

itemController.getAllItems = (req, res) => {
    try {
        const fiveItems = ['Sword', 'Shield', 'Helmet', 'Backpack', 'Lucky charm']
        return res.status(200).send(fiveItems)
    } catch (err) {
        return res.status(500).send({
            success: false,
            message: 'Error getting items.',
            error: err,
          })
    } finally {
        const dateNow = new Date
        const clientIP = req.headers['x-forwarded-for'] || req.socket.remoteAddress || null
        console.log(`${dateNow.toISOString()} | ${req.method} ${req.baseUrl} | Status: ${res.statusCode} | IP: ${clientIP} | User-Agent: ${req.headers['user-agent']}`)
    }
}

module.exports = itemController