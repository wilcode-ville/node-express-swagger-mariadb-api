// Express app configuration
const express = require('express')
const app = express()
const port = 5000

// Other dependencies
require('dotenv').config()
const swaggerUi = require('swagger-ui-express')
const swaggerJsdoc = require('swagger-jsdoc')

// API Routes
const pathToRoutesIndex = require('./api/routes/index.js')

// Swagger configuration
const pathToRoutesFiles = './api/routes/**/*.js'
const swaggerDocument = swaggerJsdoc({
    failOnErrors: true,
    definition: {
        openapi: '3.0.0',
        info: {
            title: process.env.API_NAME,
            version: '1.0.0',
        },
    },
    apis: [pathToRoutesFiles],
})

// API routes
app.use('/', pathToRoutesIndex)

// API documentation URL
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument))

// Start API server
app.listen(port, () => {
    console.log(`${process.env.API_NAME} listening on port ${port}...`)
})
