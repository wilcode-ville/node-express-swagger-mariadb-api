const mariadb = require('mariadb')

const databaseCredentials = {
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    connectionLimit: 5
}

// Create a database connection pool
const pool = mariadb.createPool(databaseCredentials)

module.exports = pool
