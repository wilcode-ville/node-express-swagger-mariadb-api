# express-swagger-api

## Getting started

To make it easy for you to get started with this Node.js, Express.js, Swagger & MariaDB API, here's a installation step by step list.

## 1. Clone the project

First we need to clone the project to your machine and navigate to the project directory.

```
<First navigate to the directory where you want the project to be installed>
git clone git@gitlab.com:wilcode-ville/node-express-swagger-mariadb-api.git
cd node-express-swagger-mariadb-api
```

## 2. Install node packages

Make sure you have Node.js installed on your machine. It is recommended to always use active/maintenance LTS version of Node.js (you can check the LTS schedule here: https://nodejs.org/en/about/releases/).

```
node --version
```

After you have confirmed the Node.js installation, you can install the required node packages with the following command:

```
npm install
```

## 3. Edit the .env file

There should be an .env file in the root of the project directory. It has all the needed parameters, but you should edit the values to match your own environment. If for some reason the .env file isn't there, you can create one from this template with copy and paste:

```
API_NAME=Express API
DB_HOST=localhost
DB_PORT=3306
DB_USER=root
DB_PASSWORD=root
DB_NAME=test
```

## 4. Run the API

We are now ready to run the API. You can do so with the following commands depending on your environment:

Development:
```
npm run dev
```

Production:
```
npm run prod
```

The difference between these two is that the development command starts the API with a package called nodemon. Nodemon watches for changes in your project files and restarts every time you make a change. In production this does not happen as the production command starts the API with the standard "node server.js" command.


## 5. Confirming that API works (w/ development URL's)

After you have started the API and you have a database up and running, you can navigate to http://localhost:5000/api. This is the root URL of the API and it should give you a response that confirms your API is online and it is connected to a MariaDB database.

## 6. Swagger API documentation

Swagger generates a live HTML documentation for the API, which makes the development and documenation process go hand in hand. The documentation can be found from http://localhost:5000/api-docs/. Whenever you edit or add new endpoints to the API, make sure you update Swagger as well.

In addition to great documenting, Swagger also provides awesome testing tool for your API endpoints. Via the docs, you can select an endpoint and test it directly from the HTML documentation. Just click the button "Try it out", pass arguments if needed and finally execute the request. Voilà!

## Use in production

This projects is still quite fresh and it has not been used in production. There are at least two things that need to be adjusted before a qualified production setup can be done.

- Setting up SSL connection
- Running the project in the background (could be done with package called forever or simply using Docker)
- Log saving

## Project status
This project is not ready and will be developed by its creator through time. Any time estimates are not currently available.

## Contributors

Thanks goes to these (this) awesome people:

<table>
    <tr>
        <td align="center">
            <a href="https://gitlab.com/wilcode-ville"><img src="https://gitlab.com/uploads/-/system/user/avatar/9878329/avatar.png?width=192" width="100px;" alt=""/><br /><sub><b>Ville Rahkonen</b></sub></a>
            <br />
        </td>
    </tr>
</table>


## License

ISC License

Copyright 2022 Wilcode Oy

Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted, provided that the above copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
